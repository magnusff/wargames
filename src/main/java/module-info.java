module no.edu.ntnu.idatt2001.magnusff.wargames {
    requires javafx.controls;
    requires javafx.fxml;


    opens no.edu.ntnu.idatt2001.magnusff.wargames to javafx.fxml;
    exports no.edu.ntnu.idatt2001.magnusff.wargames;
    exports no.edu.ntnu.idatt2001.magnusff.wargames.controllers;
    opens no.edu.ntnu.idatt2001.magnusff.wargames.controllers to javafx.fxml;
    exports no.edu.ntnu.idatt2001.magnusff.wargames.units;
    opens no.edu.ntnu.idatt2001.magnusff.wargames.units to javafx.fxml;
}