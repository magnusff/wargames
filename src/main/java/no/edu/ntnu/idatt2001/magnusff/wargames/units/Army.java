package no.edu.ntnu.idatt2001.magnusff.wargames.units;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Collectors;

public class Army {
    private String name;
    private ArrayList<Unit> units = new ArrayList<>();
    private Random random = new Random();

    /**
     * A constructor for Army with a given name.
     *
     * @param name = the name of the army.
     */
    public Army(String name) {
        this.name = name;
    }

    /**
     * A constructor for Army with a given name and units.
     *
     * @param name  = the name of the army.
     * @param units = a list of units in the army.
     */
    public Army(String name, ArrayList<Unit> units) {
        this.name = name;
        this.units = units;
    }

    public Army(Army army) {
        this.name = army.getName();
        for (Unit unit : army.getAllUnits()) {
            this.units.add(UnitFactory.createUnit(unit.getClass().getSimpleName(), unit.getName(), unit.getHealth()));
        }
    }

    /**
     * A get-method for the army´s name.
     *
     * @return = the name of the army.
     */

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * A method to add a unit to the list of units in the army.
     *
     * @param unit = a single unit in the army.
     */
    public void add(Unit unit) throws IllegalArgumentException {
        if (unit == null){
            throw new IllegalArgumentException("The unit is of wrong unitType");
        }
        units.add(unit);
    }

    /**
     * A method to add all units in a list to the army.
     *
     * @param units = a single unit in the army.
     */
    public void addAll(ArrayList<Unit> units) {
        this.units.addAll(units);
    }

    /**
     * A method to remove a unit from a list of units.
     *
     * @param unit = a single unit in the army.
     * @return
     */
    public boolean remove(Unit unit) {
        return units.remove(unit);
    }

    /**
     * A boolean to check if a list or army has units in it.
     *
     * @return True if the list/army has units it in, false if not.
     */
    public boolean hasUnits() {
        return !units.isEmpty();
    }

    /**
     * A get-method to get all units in a list of units.
     *
     * @return The units in the list.
     */
    public ArrayList<Unit> getAllUnits() {

        return this.units;
    }

    /**
     * A get-method to get a random unit in a list of units.
     *
     * @return A random unit from the list.
     */
    public Unit getRandomUnit() {
        return this.units.get(random.nextInt(units.size()));
    }

    /**
     * A toString-method that returns the name and the units in an army.
     *
     * @return Name and units in an army as a String.
     */

    @Override
    public String toString() {
        return name + "\n" + units;
    }

    /**
     * a Boolean to check if the field values are the same.
     *
     * @param o = The object that is checked
     * @return True if the field values are the same, false if not.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Army army = (Army) o;
        return Objects.equals(name, army.name) && Objects.equals(units, army.units);
    }

    /**
     * hashCode-method to put the army´s in a system based on int-values.
     *
     * @return hashcode of an army with its name and units.
     */
    @Override
    public int hashCode() {
        return Objects.hash(name, units);
    }

    /**
     * get-method for the list of infantryUnits.
     * @return all infantryUnits in the list.
     */

    public List<Unit> getInfantryUnits() {
        return units.stream().filter(p -> p instanceof InfantryUnit).collect(Collectors.toList());

    }
    /**
     * get-method for the list of rangedUnits.
     * @return all rangedUnits in the list.
     */

    public List<Unit> getRangedUnits() {
        return units.stream().filter(p -> p instanceof RangedUnit).collect(Collectors.toList());
    }
    /**
     * get-method for the list of cavalryUnits.
     * @return all cavalryUnits in the list.
     */

    public List<Unit> getCavalryUnits() {
        return units.stream().filter(p -> p instanceof CavalryUnit && p.getAttack() == 20).collect(Collectors.toList());
    }
    /**
     * get-method for the list of commanderUnits.
     * @return all commanderUnits in the list.
     */
    public List<Unit> getCommanderUnits() {
        return units.stream().filter(p -> p instanceof CommanderUnit).collect(Collectors.toList());
    }
}

