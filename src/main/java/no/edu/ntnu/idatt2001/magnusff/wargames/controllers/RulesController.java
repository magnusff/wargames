package no.edu.ntnu.idatt2001.magnusff.wargames.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.Parent;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public class RulesController {
    private Stage stage;
    private Scene scene;

    /**
     * Method to return to the welcomeScreen.
     * @param event user-activated action when button is clicked.
     * @throws IOException
     */
    @FXML
    public void backToWelcomeScreen(ActionEvent event) throws IOException{
        URL url = new File("src/main/resources/no/edu/ntnu/idatt2001/magnusff/wargames/welcomeScreen.fxml").toURI().toURL();
        Parent root = FXMLLoader.load(url);
        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
}
