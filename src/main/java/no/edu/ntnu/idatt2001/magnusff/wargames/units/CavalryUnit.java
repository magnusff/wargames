package no.edu.ntnu.idatt2001.magnusff.wargames.units;

public class CavalryUnit extends Unit {
    private int attackbonus;
    private int resistbonus;

    /**
     * Constructor
     * @param name name of the cavalryUnit
     * @param health health of the cavalryUnit
     * @param attack attack of the cavalryUnit
     * @param armor armor of the cavalryUnit
     */
    public CavalryUnit(String name, int health, int attack, int armor) {
        super.unit(name, health, attack, armor);
    }

    /**
     * Constructor for the cavalryUnit with attack- and resist-bonus.
     * @param name = name of the cavalryUnit
     * @param health = health of the cavalryUnit
     */
    public CavalryUnit(String name, int health){
        this(name, health, 20, 12);
        this.attackbonus = 10;
        this.resistbonus = 1;
    }

    /**
     * If/else to return correct AttackBonus for the cavalryUnit.
     * @return the correct AttackBonus based on number of attacks.
     */
    public int getAttackBonus(String terrain){
        if (attackbonus > 6){
            attackbonus -= 4;
        } else if(attackbonus > 2){
            attackbonus -= 4;
        }
        if (terrain.toLowerCase().trim().equals("plain")){
            return attackbonus + 3;
        }
        return attackbonus;
    }

    /**
     * get-method for the cavalryUnits resistbonus
     * @return the resistbonus of the unit
     */
    public int getResistBonus(String terrain){
        if(terrain.toLowerCase().trim().equals("forrest")){
            return 0;
        }else
            return resistbonus;
    }
}