package no.edu.ntnu.idatt2001.magnusff.wargames.controllers;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import no.edu.ntnu.idatt2001.magnusff.wargames.FileHandler;
import no.edu.ntnu.idatt2001.magnusff.wargames.units.Army;
import no.edu.ntnu.idatt2001.magnusff.wargames.units.Unit;
import no.edu.ntnu.idatt2001.magnusff.wargames.units.UnitFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

public class ArmyTwoController {
    private Stage stage;
    private Scene scene;
    private Parent root;
    private String name;
    private Army armyOne;
    private Army armyTwo;

    @FXML
    private TextField nameArea;
    @FXML
    private RadioButton newArmy;
    @FXML
    private RadioButton armyFromFile;

    @FXML
    private Label infantryLabel;
    @FXML
    private Label rangedLabel;
    @FXML
    private Label cavalryLabel;



    @FXML
    private Button proceed;
    @FXML
    private Button fiveMoreInfantryUnits;
    @FXML
    private Button fiveMoreRangedUnits;
    @FXML
    private Button fiveMoreCavalryUnits;
    @FXML
    private Button selectFile;
    @FXML
    private TextArea inputFile;
    @FXML
    private Button fiveLessInfantryUnits;
    @FXML
    private Button fiveLessRangedUnits;
    @FXML
    private Button fiveLessCavalryUnits;
    @FXML
    private RadioButton commanderBaggo;
    @FXML
    private RadioButton commanderHuriensh;


    @FXML
    public void initialize() {
        ToggleGroup scratchOrFile = new ToggleGroup();
        ToggleGroup commander = new ToggleGroup();
        armyFromFile.setToggleGroup(scratchOrFile);
        newArmy.setToggleGroup(scratchOrFile);
        commanderHuriensh.setToggleGroup(commander);
        commanderBaggo.setToggleGroup(commander);
        armyFromFileSelected();
        proceed.setDisable(true);
        selectFile.setVisible(false);
        inputFile.setVisible(false);
    }

    /**
     * Method to display correct labels when selecting to make a new army in the program.
     */
    @FXML
    public void newArmySelected() {
        fiveMoreInfantryUnits.setVisible(true);
        fiveLessInfantryUnits.setVisible(true);
        rangedLabel.setVisible(true);
        cavalryLabel.setVisible(true);
        infantryLabel.setVisible(true);
        fiveMoreRangedUnits.setVisible(true);
        fiveMoreCavalryUnits.setVisible(true);
        fiveLessCavalryUnits.setVisible(true);
        fiveLessRangedUnits.setVisible(true);
        fiveMoreCavalryUnits.setVisible(true);
        proceed.setDisable(false);


        selectFile.setVisible(false);
        inputFile.setVisible(false);
        unitsSelected();

    }

    /**
     * Method to display correct labels when selecting to import a file from the computers library.
     */
    @FXML
    public void armyFromFileSelected() {
        infantryLabel.setVisible(false);
        rangedLabel.setVisible(false);
        cavalryLabel.setVisible(false);
        fiveLessCavalryUnits.setVisible(false);
        fiveLessRangedUnits.setVisible(false);
        fiveLessInfantryUnits.setVisible(false);
        fiveMoreInfantryUnits.setVisible(false);
        fiveMoreRangedUnits.setVisible(false);
        fiveMoreCavalryUnits.setVisible(false);
        commanderHuriensh.setVisible(false);
        commanderBaggo.setVisible(false);
        selectFile.setVisible(true);
        inputFile.setVisible(true);
        if (inputFile.getText().isEmpty()) {
            proceed.setDisable(true);
        }else proceed.setDisable(false);
    }

    /**
     * Opens up the finder to search for the army-file to import.
     * @throws FileNotFoundException
     */
    @FXML
    private void selectFileClicked() throws FileNotFoundException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Please select a file");
        File file = fileChooser.showOpenDialog(null);

        try {
            if (!inputFile.getText().isEmpty()) {
                inputFile.setText(file.getAbsolutePath());
                proceed.setDisable(false);
                this.armyTwo = FileHandler.readFromCsv(inputFile.getText());
                nameArea.setText(this.armyTwo.getName());
            } else {inputFile.setText("You have to choose a file");}
        } catch (NullPointerException e) {
            inputFile.setText(e.getMessage());

        }

    }

    /**
     * Method to add 5 infantryUnits when button is clicked.
     * @param event user-activated action when button is clicked.
     */

    @FXML
    public void infantryUpClicked(ActionEvent event) {
        int amount = Integer.parseInt(infantryLabel.getText());
        if (event.getSource().equals(fiveMoreInfantryUnits)) {
            amount += 5;
        }
        infantryLabel.setText(String.valueOf(amount));
        unitsSelected();
    }

    /**
     * Method to remove 5 infantryUnits when button is clicked.
     * @param event user-activated action when button is clicked.
     */
    @FXML
    public void infantryDownClicked(ActionEvent event) {
        int amount = Integer.parseInt(infantryLabel.getText());
        if (event.getSource().equals(fiveLessInfantryUnits)) {
            amount -= 5;
        }
        infantryLabel.setText(String.valueOf(amount));
        unitsSelected();
    }

    /**
     * Method to add 5 rangedUnits when button is clicked.
     */
    public void rangedUpClicked(ActionEvent event) {
        int amount = Integer.parseInt(rangedLabel.getText());
        if (event.getSource().equals(fiveMoreRangedUnits)) {
            amount += 5;
        }
        rangedLabel.setText(String.valueOf(amount));
        unitsSelected();
    }

    /**
     * Method to remove 5 rangedUnits when button is clicked.
     * @param event user-activated action when button is clicked.
     */
    public void rangedDownClicked(ActionEvent event) {
        int amount = Integer.parseInt(rangedLabel.getText());
        if (event.getSource().equals(fiveLessRangedUnits)) {
            amount -= 5;
        }
        rangedLabel.setText(String.valueOf(amount));
        unitsSelected();
    }

    /**
     * Method to add 5 cavalryUnits when button is clicked.
     * @param event user-activated action when button is clicked.
     */

    public void cavalryUpClicked(ActionEvent event) {
        int amount = Integer.parseInt(cavalryLabel.getText());
        if (event.getSource().equals(fiveMoreCavalryUnits)) {
            amount += 5;
        }
        cavalryLabel.setText(String.valueOf(amount));
        unitsSelected();
    }

    /**
     * Method to remove 5 cavalryUnits when button is clicked.
     * @param event user-activated action when button is clicked.
     */
    public void cavalryDownClicked(ActionEvent event) {
        int amount = Integer.parseInt(cavalryLabel.getText());
        if (event.getSource().equals(fiveLessCavalryUnits)) {
            amount -= 5;
        }
        cavalryLabel.setText(String.valueOf(amount));
        unitsSelected();
    }

    /**
     * Enables the proceed-button when criterias are filled.
     */
    @FXML
    public void unitsSelected() {
        if (commanderBaggo.isSelected() || commanderHuriensh.isSelected()){
            proceed.setDisable(false);
        }else if (Integer.parseInt(infantryLabel.getText()) != 0 || Integer.parseInt(rangedLabel.getText()) != 0 || Integer.parseInt(cavalryLabel.getText()) != 0){
            proceed.setDisable(false);
        }else if (Integer.parseInt(infantryLabel.getText()) == 0 || Integer.parseInt(rangedLabel.getText()) == 0 || Integer.parseInt(cavalryLabel.getText()) == 0){
            proceed.setDisable(true);
        }
    }

    /**
     * Method to set name of the army, and make a list of units if newArmy is selected. If armyFromFile is selected, it sets the correct name to the army. Lastly it proceeds to the next fxml.
     * @param event user-activated action when button is clicked.
     * @throws IOException
     */
    @FXML
    public void proceedClicked(ActionEvent event) throws IOException{
        name = nameArea.getText();
        if (newArmy.isSelected()){
            ArrayList<Unit> totalUnits = new ArrayList<>();

            totalUnits.addAll(UnitFactory.createListOfUnits("InfantryUnit", "Baggo", 50, Integer.parseInt(infantryLabel.getText())));
            totalUnits.addAll(UnitFactory.createListOfUnits("RangedUnit", "Puero", 30, Integer.parseInt(rangedLabel.getText())));
            totalUnits.addAll(UnitFactory.createListOfUnits("CavalryUnit", "Mikje", 70, Integer.parseInt(cavalryLabel.getText())));
            armyTwo = new Army(name, totalUnits);
        }else if (armyFromFile.isSelected()){
            this.armyTwo.setName(name);
        }
        URL url = new File("scr/main/resources/no/edu/ntnu/idatt2001/magnusff/wargames/ready.fxml").toURI().toURL();
        FXMLLoader loader = new FXMLLoader(url);
        root = loader.load();
        ReadyController readyController = loader.getController();
        readyController.setArmyOne(this.armyOne);
        readyController.setArmyTwo(this.armyTwo);

        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Set-method for ArmyOne.
     * @param armyOne army one.
     */
    public void setArmyOne(Army armyOne){
        this.armyOne = armyOne;
    }
}