package no.edu.ntnu.idatt2001.magnusff.wargames.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import no.edu.ntnu.idatt2001.magnusff.wargames.units.Army;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public class ReadyController {
    private Army armyOne;
    private Army armyTwo;
    private String terrain;

    private Stage stage;
    private Scene scene;
    private Parent root;

    @FXML
    private Button battle;
    @FXML
    private Label armyOneCommander;
    @FXML
    private Label armyOneInfantry;
    @FXML
    private Label armyOneRanged;
    @FXML
    private Label armyOneCavalry;
    @FXML
    private Label armyTwoCommander;
    @FXML
    private Label armyTwoInfantry;
    @FXML
    private Label armyTwoRanged;
    @FXML
    private Label armyTwoCavalry;
    @FXML
    private RadioButton forest;
    @FXML
    private RadioButton hill;
    @FXML
    private RadioButton plains;
    @FXML
    private Label armyOneName;
    @FXML
    private Label armyTwoName;

    @FXML
    public void initialize() {
        ToggleGroup toggleGroup = new ToggleGroup();
        forest.setToggleGroup(toggleGroup);
        hill.setToggleGroup(toggleGroup);
        plains.setToggleGroup(toggleGroup);
        battle.setDisable(true);
    }

    /**
     * Method to set the terrain based on the button clicked.
     */
    @FXML
    public void terrainClicked() {
        battle.setDisable(false);
        if (forest.isSelected()) {
            terrain = "forest";
        } else if (hill.isSelected()) {
            terrain = "hill";
        } else if (plains.isSelected()) {
            terrain = "plain";
        }
    }

    /**
     * Set-method for army one with its units.
     * @param army army
     */
    @FXML
    public void setArmyOne(Army army) {
        this.armyOne = army;
        armyOneName.setText(armyOne.getName());
        armyOneCommander.setText(String.valueOf(armyOne.getCommanderUnits().size()));
        armyOneInfantry.setText(String.valueOf(armyOne.getInfantryUnits().size()));
        armyOneRanged.setText(String.valueOf(armyOne.getRangedUnits().size()));
        armyOneCavalry.setText(String.valueOf(armyOne.getCavalryUnits().size()));
    }

    /**
     * Set-method for army two with its units.
     * @param army army
     */
    @FXML
    public void setArmyTwo(Army army) {
        this.armyTwo = army;
        armyTwoName.setText(armyTwo.getName());
        armyTwoCommander.setText(String.valueOf(armyTwo.getCommanderUnits().size()));
        armyTwoInfantry.setText(String.valueOf(armyTwo.getInfantryUnits().size()));
        armyTwoRanged.setText(String.valueOf(armyTwo.getRangedUnits().size()));
        armyTwoCavalry.setText(String.valueOf(armyTwo.getCavalryUnits().size()));
    }

    /**
     * Method to proceed to the battle-screen.
     * @param event user-activated action when button is clicked.
     * @throws IOException
     * @throws InterruptedException
     */
    @FXML
    public void battleClicked(ActionEvent event)throws IOException, InterruptedException{
        URL url = new File("src/main/resources/no/edu/ntnu/idatt2001/magnusff/wargames/battle.fxml").toURI().toURL();
        FXMLLoader loader = new FXMLLoader(url);
        root = loader.load();

        BattleController battleController = loader.getController();
        battleController.transferVariables(armyOne,armyTwo,terrain);
        battleController.startBattle();

        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
}
