package no.edu.ntnu.idatt2001.magnusff.wargames.controllers;

import javafx.animation.Animation;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import no.edu.ntnu.idatt2001.magnusff.wargames.Battle;
import no.edu.ntnu.idatt2001.magnusff.wargames.FileHandler;
import no.edu.ntnu.idatt2001.magnusff.wargames.units.Army;


import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

public class BattleController {
    private Army armyOne;
    private Army armyTwo;
    private Army copyArmyOne;
    private Army copyArmyTwo;
    private String terrain;
    private Battle battle;
    private Army winner;
    private ArrayList<Integer> armyOneHealthOverTime;
    private ArrayList<Integer> armyTwoHealthOverTime;

    private Stage stage;
    private Scene scene;
    private Parent root;

    @FXML
    private Label armyOneHealth;
    @FXML
    private  Label armyTwoHealth;
    @FXML
    private Label armyOneName;
    @FXML
    private Label armyTwoName;
    @FXML
    private ProgressBar armyOneBar;
    @FXML
    private ProgressBar armyTwoBar;
    @FXML
    private Label showWinner;
    @FXML
    private Button mainMenu;
    @FXML
    private Button saveWinningArmy;
    @FXML
    private Button restartBattle;

    @FXML
    public void initialize(){
        showWinner.setVisible(false);
        mainMenu.setVisible(false);
        saveWinningArmy.setVisible(false);
        restartBattle.setVisible(false);
    }

    /**
     * Constructor to transfer the values of the armies.
     * @param armyOne army one
     * @param armyTwo army two
     * @param terrain the terrain set for the battle.
     */
    public void transferVariables(Army armyOne, Army armyTwo, String terrain){
        this.armyOne = armyOne;
        this.armyTwo = armyTwo;
        this.terrain = terrain;
        armyOneName.setText(armyOne.getName());
        armyTwoName.setText(armyTwo.getName());
    }

    /**
     * Method to run the battle and show a live health-bar for the remaining tropps of the armies. Sleeps the pace to make it readable for humans.
     */
    public void startBattle(){
        copyArmyOne = new Army(armyOne);
        copyArmyTwo = new Army(armyTwo);
        battle = new Battle(armyOne, armyTwo);
        winner = battle.simulate(this.terrain);
        armyOneHealthOverTime = battle.getArmyOneHealthLive();
        armyTwoHealthOverTime = battle.getArmyTwoHealthLive();
        armyOneHealth.setText(armyOneHealthOverTime.get(0) + "/" + armyOneHealthOverTime.get(0));
        armyTwoHealth.setText(armyTwoHealthOverTime.get(0) + "/" + armyTwoHealthOverTime.get(0));

        Runnable task = () -> {
            try {
                for (int i = 1; i < armyOneHealthOverTime.size(); i++){
                    final int j = 1;
                    Thread.sleep(200);
                    Platform.runLater(() -> {
                        armyOneHealth.setText(armyOneHealthOverTime.get(j) + "/" + armyOneHealthOverTime.get(0));
                        armyTwoHealth.setText(armyTwoHealthOverTime.get(j) + "/" + armyTwoHealthOverTime.get(0));
                        armyOneBar.setProgress(armyOneHealthOverTime.get(j) / (float) armyOneHealthOverTime.get(0));
                        armyTwoBar.setProgress(armyTwoHealthOverTime.get(j) / (float) armyTwoHealthOverTime.get(0));
                    });
                }
                Platform.runLater(() -> {
                    showWinner.setText(winner.getName() + " won the Battle!");
                    showWinner.setVisible(true);
                    mainMenu.setVisible(true);
                    saveWinningArmy.setVisible(true);
                    restartBattle.setVisible(true);
                });
            } catch (InterruptedException e){
                e.printStackTrace();
            }
        };
        new Thread(task).start();
    }

    /**
     * Method to return to the welcomeScreen.
     * @param event user-activated action when button is clicked.
     * @throws IOException
     */
    @FXML
    public void backToMainMenu(ActionEvent event)throws IOException{
        URL url = new File("src/main/resources/no/edu/ntnu/idatt2001/magnusff/wargames/welcomeScreen.fxml").toURI().toURL();
        FXMLLoader loader = new FXMLLoader(url);
        root = loader.load();

        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Method to save the winning army after the battle.
     * @param event user-activated action when button is clicked.
     * @throws IOException
     */
    @FXML
    public void goToSaveWinningArmy(ActionEvent event) throws IOException{
        FileChooser fc = new FileChooser();
        File file = fc.showSaveDialog(null);
        if(winner.getName().equals(armyOne.getName())){
            FileHandler.writeToCsv(armyOne,file.getPath() + ".csv");
        }else{
            FileHandler.writeToCsv(armyTwo,file.getPath() + ".csv");
        }
    }
    @FXML
    public void restartBattle(ActionEvent event) throws IOException{
        URL url = new File("src/main/resources/no/edu/ntnu/idatt2001/magnusff/wargames/ready.fxml").toURI().toURL();
        FXMLLoader loader = new FXMLLoader(url);
        root = loader.load();

        ReadyController readyController = loader.getController();
        readyController.setArmyOne(this.copyArmyOne);
        readyController.setArmyTwo(this.copyArmyTwo);

        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
}
