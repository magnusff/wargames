package no.edu.ntnu.idatt2001.magnusff.wargames.units;

public abstract class Unit {
    private String name;
    private int health;
    private int attack;
    private int armor;

    /**
     * Get-method for the Unit´s name.
     * @return name of the Unit.
     */
    public String getName() {
        return name;
    }

    /**
     * Get-method for the health-stat of the Unit.
     * @return health of the Unit.
     */
    public int getHealth() {
        return health;
    }

    /**
     * Get-method for the attack-stat of the Unit.
     * @return attack-stat of the Unit.
     */
    public int getAttack() {
        return attack;
    }

    /**
     * Get-method for the armor-stat of the Unit.
     * @return armor-stat of the Unit.
     */
    public int getArmor() {
        return armor;
    }

    /**
     * Set-method for the health-stat of the Unit.
     * @param health = the Unit´s health-stat.
     */
    public void setHealth(int health) {
        this.health = health;
    }

    /**
     * toString-method for the Unit-class.
     * @return health, attack and armor in a readable way.
     */
    @Override
    public String toString() {
        return "Remaining solider : " + name +
                " , Health = " + health + " , Attack = " + attack + " , Armor = " + armor + "\n";
    }

    /**
     * Constructor for the units.
     * @param name = name of the unit.
     * @param health = health-stat of the unit.
     * @param attack = attack-stat of the unit.
     * @param armor = armor-stat of the unit.
     */
    public void unit(String name, int health, int attack, int armor){
        if (health <= 0){
            throw new IllegalArgumentException("Health cant be 0 or lower");
        }
        if(attack <= 0){
            throw new IllegalArgumentException("Attack cant be 0 or lower");
        }
        if(armor <= 0){
            throw new IllegalArgumentException("Armor cant be 0 or lower");
        }
        if (name.isBlank()){
            throw new IllegalArgumentException("The solider needs a name!");
        }
        this.name = name;
        this.health = health;
        this.attack = attack;
        this.armor = armor;
    }

    /**
     * Attack-method for the units.
     * @param opponent = The unit that is attacked by the method.
     */
    public void attack(Unit opponent, String terrain){
        opponent.setHealth((opponent.health - (this.attack + this.getAttackBonus(terrain)) + (opponent.armor + opponent.getResistBonus(terrain))));
    }

    /**
     * Abstract get-method for the attackBonus of the units.
     * @return attackbonus of the Unit.
     */
    public abstract int getAttackBonus(String terrain);

    /**
     * Abstract get-method for the resistBonus of the units.
     * @return resistBonus of the Unit.
     */
    public abstract int getResistBonus(String terrain);

}


