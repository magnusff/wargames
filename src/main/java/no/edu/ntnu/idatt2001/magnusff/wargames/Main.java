package no.edu.ntnu.idatt2001.magnusff.wargames;

import no.edu.ntnu.idatt2001.magnusff.wargames.units.*;

public class Main {

    /**
     * Creates two army´s and adds 500 infantryUnits, 100 cavalryUnits and 200 rangedUnits to both army´s. Used throughout the project for learning purposes.
     */
    public static void main(String[] args) {
        Army Baggo = new Army("Baggo-army");
        Army Puero = new Army("Ronlend-army");

        for(int i = 0; i < 501; i++){
            Baggo.add(new InfantryUnit("vanligbaggo", 100));
            Puero.add(new InfantryUnit("vanligronlend", 100));
    }
        for(int i = 0; i < 101; i++){
            Baggo.add(new CavalryUnit("Høvlebaggo", 100));
            Puero.add(new CavalryUnit("Høvleronlend", 100));
        }
        for(int i = 0; i < 201; i++){
            Baggo.add(new RangedUnit("Avstandsbaggo", 100));
            Puero.add(new RangedUnit("AvstandsRonlend", 100));
        }
        Baggo.add(new CommanderUnit("Baggo OP", 180));
        Puero.add(new CommanderUnit("Ronlend OP", 180));

        Battle battle = new Battle(Baggo, Puero);
        System.out.println(battle.simulate("Forrest"));
    }
}
