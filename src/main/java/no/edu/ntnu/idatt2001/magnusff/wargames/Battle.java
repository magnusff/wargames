package no.edu.ntnu.idatt2001.magnusff.wargames;

import no.edu.ntnu.idatt2001.magnusff.wargames.units.Army;
import no.edu.ntnu.idatt2001.magnusff.wargames.units.Unit;

import java.util.ArrayList;

public class Battle {
    private Army armyOne;
    private Army armyTwo;
    private ArrayList<Integer> armyOneHealthLive = new ArrayList<Integer>();
    private ArrayList<Integer> armyTwoHealthLive = new ArrayList<Integer>();

    /**
     * Constructor for Battle.
     *
     * @param armyOne = The first army in the Battle.
     * @param armyTwo = The second army in the Battle.
     */
    public Battle(Army armyOne, Army armyTwo) {
        this.armyOne = armyOne;
        this.armyTwo = armyTwo;
    }

    /**
     * Simulate-method to run the Battle between the army´s.
     *
     * @return The army which still hasUnits left in their army. The winner.
     */
    public Army simulate(String terrain) {
        while (true) {
            Unit attacker1 = armyOne.getRandomUnit();
            Unit defender1 = armyTwo.getRandomUnit();
            attacker1.attack(defender1, terrain);

            if (defender1.getHealth() <= 0) {
                armyTwo.remove(defender1);
                if (!armyTwo.hasUnits()) {
                    break;
                }
            }


            Unit attacker2 = armyTwo.getRandomUnit();
            Unit defender2 = armyOne.getRandomUnit();
            attacker2.attack(defender2, terrain);

            if (defender2.getHealth() <= 0) {
                armyOne.remove(defender2);
                if (!armyOne.hasUnits()) {
                    break;
                }
            }

        }

        if (armyOne.hasUnits()) {
            return armyOne;
        } else {
            return armyTwo;
        }

    }


    /**
     * ToString-method for the Battle-class to make the return readable.
     *
     * @return armyOne and armyTwo.
     */
    @Override
    public String toString() {
        return "armyOne=" + armyOne +
                ", armyTwo=" + armyTwo;
    }

    public ArrayList<Integer> getArmyOneHealthLive() {
        return armyOneHealthLive;
    }

    public ArrayList<Integer> getArmyTwoHealthLive() {
        return armyTwoHealthLive;
    }
}

