package no.edu.ntnu.idatt2001.magnusff.wargames.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.fxml.FXMLLoader;

import java.io.File;
import java.io.IOException;
import java.net.URL;


public class WelcomeScreen_Controller {
    private Stage stage;
    private Scene scene;
    private Parent root;

    @FXML
    private Button quit;

    /**
     * Method to proceed to creating army one for the battle.
     * @param event user-activated action when button is clicked.
     * @throws IOException
     */
    @FXML
    protected void onStartButtonClick(ActionEvent event) throws IOException {
        URL url = new File("src/main/resources/no/edu/ntnu/idatt2001/magnusff/wargames/armyOne.fxml").toURI().toURL();
        Parent root = FXMLLoader.load(url);
        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Method to quit the application.
     */
    @FXML
    private void quitApplication(){
        Stage stage = (Stage) quit.getScene().getWindow();
        stage.close();
    }

    /**
     * Method to proceed to the rules-screen.
     * @param event user-activated action when button is clicked.
     * @throws IOException
     */
    public void onRulesClicked(ActionEvent event) throws IOException{
        URL url = new File("src/main/resources/no/edu/ntnu/idatt2001/magnusff/wargames/rules.fxml").toURI().toURL();
        Parent root = FXMLLoader.load(url);
        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
}