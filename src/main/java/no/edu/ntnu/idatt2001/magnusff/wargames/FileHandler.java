package no.edu.ntnu.idatt2001.magnusff.wargames;

import no.edu.ntnu.idatt2001.magnusff.wargames.units.*;

import java.io.*;
import java.util.ArrayList;

public class FileHandler {

    /**
     * Converting UnitType, Name and Health to String.
     * @param army The Army
     * @return stringbuilder with UnitType, Name and Health.
     */
        public static String convertToString(Army army) {
            StringBuilder sb = new StringBuilder();
            sb.append(army.getName() + "\r\n");
            for (Unit unit : army.getAllUnits()) {
                sb.append(getUnitType(unit) + ",");
                sb.append(unit.getName() + ",");
                sb.append(unit.getHealth());
                sb.append("\r\n");
            }
            return sb.toString();
        }

    /**
     * Writing to Csv-file
     * @param filePath The path to the file.
     * @throws IOException
     */

        public static void writeToCsv(Army army, String filePath) throws IOException {
            String string = convertToString(army);
            try (FileWriter file = new FileWriter(filePath);
                 PrintWriter output = new PrintWriter(file)) {
                output.print(string);
            } catch (Exception e) {
                e.getStackTrace();
            }
        }

    /**
     * Reading from a Csv-file
     * @param filePath The path to the file to be read.
     * @return null.
     * @throws FileNotFoundException Exception thrown if the file to be read is not found.
     * @throws NumberFormatException Exception thrown if the format to be read is wrong.
     */

        public static Army readFromCsv(String filePath) throws FileNotFoundException, NumberFormatException{
            String line = "";
            String splitBy = ",";
            ArrayList<Unit> returnList = new ArrayList<>();

            try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
                Army army = new Army(br.readLine().split(splitBy)[0]);

                while ((line = br.readLine()) != null) {
                    String[] unit = line.split(splitBy);
                    returnList.add(makeUnitFromList(unit));
                }
                army.addAll(returnList);
                return army;
            } catch (IOException e) {
                System.out.println("Error reading from file: " + e);
            }
            return null;
        }

    /**
     * Write the correct unitType based on the Unit.
     * @param unit A Unit.
     * @return the unitType.
     */

    public static String getUnitType(Unit unit) {
            String type = "";
            if(unit instanceof InfantryUnit) {
                type = "Infantry Unit";
            } else if(unit instanceof RangedUnit) {
                type =  "Ranged Unit";
            } else if(unit instanceof CommanderUnit) {
                type = "Commander Unit";
            } else if(unit instanceof CavalryUnit) {
                type = "Cavalry Unit";
            }
            return type;
        }

    /**
     * Makes a unit from a list based on unitType.
     * @param unitInfo The variable-stats for the Unit.
     * @return null.
     */

    public static Unit makeUnitFromList(String[] unitInfo) {
        return switch (unitInfo[0]) {
            case "Infantry Unit" -> (new InfantryUnit(unitInfo[1], Integer.parseInt(unitInfo[2])));
            case "Ranged Unit" -> (new RangedUnit(unitInfo[1], Integer.parseInt(unitInfo[2])));
            case "Cavalry Unit" -> (new CavalryUnit(unitInfo[1], Integer.parseInt(unitInfo[2])));
            case "Commander Unit" -> (new CommanderUnit(unitInfo[1], Integer.parseInt(unitInfo[2])));
            default -> null;
        };
    }
    }
