package no.edu.ntnu.idatt2001.magnusff.wargames.units;

public class CommanderUnit extends CavalryUnit {

    /**
     * Constructor for the commanderUnit
     * @param name = name of the commanderUnit
     * @param health = health of the commanderUnit
     * @param attack = attack of the commanderUnit
     * @param armor = armor of the commanderUnit
     */
    public CommanderUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * Constructor for the commanderUnit with attack and health-values.
     * @param name = name of the commanderUnit
     * @param health = health of the commanderUnit
     */
    public CommanderUnit(String name, int health) {
        this(name, health, 25, 15);
    }

}

