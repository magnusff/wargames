package no.edu.ntnu.idatt2001.magnusff.wargames.units;

public class InfantryUnit extends Unit {

    public int attackbonus;
    public int resistbonus;

    /**
     * Constructor for the infantryUnit
     * @param name = name of the infantryUnit
     * @param health = health of the infantryUnit
     * @param attack = attack of the infantryUnit
     * @param armor = armor of the infantryUnit
     */
    public InfantryUnit(String name, int health, int attack, int armor) {
        super.unit(name, health, attack, armor);
    }

    /**
     * Constructor for the infantryUnit with attack- and resistbonus-values.
     * @param name = name of the infantryUnit
     * @param health = health of the infantryUnit
     */
    public InfantryUnit(String name, int health){
        this(name, health, 15, 10);
        this.attackbonus = 2;
        this.resistbonus = 1;
    }

    @Override
    /**
     * get-method for the AttackBonus of the infantryUnit.
     * @return the AttackBonus-value of 2.
     */

    public int getAttackBonus(String terrain) {
        if (terrain.toLowerCase().trim().equals("forrest")){
            return attackbonus + 2;
        }else
        return attackbonus;
    }

    /**
     * get-method for the ResistBonus of the infantryUnit.
     * @return 1.
     */
    public int getResistBonus(String terrain){
        if(terrain.toLowerCase().trim().equals("forrest")){
            return resistbonus + 2;
        }else
        return resistbonus;
    }
}

