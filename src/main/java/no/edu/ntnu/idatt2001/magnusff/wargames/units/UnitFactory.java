package no.edu.ntnu.idatt2001.magnusff.wargames.units;

import java.util.ArrayList;

public class UnitFactory {

    /**
     * Creates a Unit based on the String provided.
     * @param unitType The type of Unit.
     * @param name The name of the Unit.
     * @param health The health of the Unit.
     * @return unit created based on the input.
     */
    public static Unit createUnit(String unitType, String name, int health) {
        Unit unit = null;
        switch (unitType) {
            case "InfantryUnit" -> unit = new InfantryUnit(name, health);
            case "RangedUnit" -> unit = new RangedUnit(name, health);
            case "CavalryIUnit" -> unit = new CavalryUnit(name, health);
            case "CommanderUnit" -> unit = new CommanderUnit(name, health);
        }
        return unit;
    }

    /**
     * Creates a list of Units based on the String provided.
     * @param unitType The type of Unit.
     * @param name The name of the Unit.
     * @param health The health of the Unit.
     * @param n Number of units.
     * @return The list of Units created based on the input.
     */
    public static ArrayList<Unit> createListOfUnits(String unitType, String name, int health, int n) {

        ArrayList<Unit> createdUnits = new ArrayList<>();


        switch (unitType) {
            case "InfantryUnit":
                for (int i = 0; i < n; i++) {
                    createdUnits.add(new InfantryUnit(name, health));
                }
            case "RangedUnit":
                for (int i = 0; i < n; i++) {
                    createdUnits.add(new RangedUnit(name, health));
                }
            case "CavalryUnit":
                for (int i = 0; i < n; i++) {
                    createdUnits.add(new CavalryUnit(name, health));
                }
            case "CommanderUnit":
                for (int i = 0; i < n; i++) {
                    createdUnits.add(new CommanderUnit(name, health));
                }
        } return createdUnits;
    }
}