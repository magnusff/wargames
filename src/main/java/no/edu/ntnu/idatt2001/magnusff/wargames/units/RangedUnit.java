package no.edu.ntnu.idatt2001.magnusff.wargames.units;

public class RangedUnit extends Unit {
    private int resistBonus;
    private int attackBonus;

    /**
     * Constructor for the rangedUnit
     *
     * @param name   = name of the rangedUnit
     * @param health = health of the rangedUnit
     * @param attack = attack of the rangedUnit
     * @param armor  = armor of the rangedUnit
     */
    public RangedUnit(String name, int health, int attack, int armor) {
        super.unit(name, health, attack, armor);
        this.attackBonus = 3;
        this.resistBonus = 8;
    }

    /**
     * Constructor for the rangedUnit with attack- and armor-values.
     *
     * @param name   = name of the rangedUnit
     * @param health = health of the rangedUnit
     */
    public RangedUnit(String name, int health) {
        this(name, health, 15, 8);
    }

    /**
     * Get-method for the rangedUnits attackBonus.
     *
     * @return the attackBonus of the rangedUnit.
     */
    @Override
    public int getAttackBonus(String terrain) {
        if (terrain.toLowerCase().trim().equals("hill")){
            return attackBonus + 2;
        }else if (terrain.toLowerCase().trim().equals("forrest")){
            return attackBonus -2;
        }
        return attackBonus;
    }


    /**
     * Get-method for the rangedUnits resistBonus which is reduced when hit multiple times.
     *
     * @return the resistBonus of the rangedUnit.
     */
    public int getResistBonus(String terrain) {
         if(resistBonus > 6){
            resistBonus -= 2;
        }else if(resistBonus > 4){
            resistBonus -= 2;
        }else if(resistBonus > 2){
            resistBonus -= 2;
        }
        return resistBonus;
    }
}



