package no.edu.ntnu.idatt2001.magnusff.wargames;

import no.edu.ntnu.idatt2001.magnusff.wargames.units.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FilehandlerTest {
    @Test
    @DisplayName("Convert To String")
    public void convertToString() {
        Army army = new Army("Army one");
        InfantryUnit unit_1 = new InfantryUnit("Kåre", 15);
        RangedUnit unit_2 = new RangedUnit("Snikka", 20 );
        army.add(unit_1);
        army.add(unit_2);
        System.out.println(army);
    }
    @Test
    @DisplayName("Write To Csv-File")
    public void writeToCsv() throws IOException {
        Army army = new Army("Army one");
        InfantryUnit unit_1 = new InfantryUnit("Kåre", 15);
        RangedUnit unit_2 = new RangedUnit("Snikka", 20 );
        army.add(unit_1);
        army.add(unit_2);
        FileHandler.writeToCsv(army, "src/main/resources/no/edu/ntnu/idatt2001/magnusff/wargames/ArmyWrite.csv");
    }

    @Nested
    @DisplayName("Reading from file")
    class readingFromFile {

        @Test
        @DisplayName("Army has units")
        public void armyHasUnits() throws FileNotFoundException {
            Army army = FileHandler.readFromCsv("src/main/resources/no/edu/ntnu/idatt2001/magnusff/wargames/ArmyWrite.csv");
            assertTrue(army.hasUnits());
        }

        @Test
        @DisplayName("Army has name")
        public void armyHasName() throws FileNotFoundException {
            Army army = FileHandler.readFromCsv("src/main/resources/no/edu/ntnu/idatt2001/magnusff/wargames/ArmyWrite.csv");
            assertEquals("Army one", army.getName().trim());
        }

        @Test
        @DisplayName("InfantryUnit is of correct type")
        public void InfantryUnitIsOfCorrectType() throws IOException {
            Army writeArmy = new Army("Army one");
            InfantryUnit unit_1 = new InfantryUnit("Snikka", 15);
            writeArmy.add(unit_1);
            FileHandler.writeToCsv(writeArmy, "src/main/resources/no/edu/ntnu/idatt2001/magnusff/wargames/ArmyWrite.csv");

            Army readArmy = FileHandler.readFromCsv("src/main/resources/no/edu/ntnu/idatt2001/magnusff/wargames/ArmyWrite.csv");
            assertTrue(readArmy.getInfantryUnits().get(0) instanceof InfantryUnit);
        }

        @Test
        @DisplayName("RangedUnit is of correct type")
        public void RangedUnitIsOfCorrectType() throws IOException {
            Army writeArmy = new Army("Army one");
            RangedUnit unit_1 = new RangedUnit("Kåre", 15);
            writeArmy.add(unit_1);
            FileHandler.writeToCsv(writeArmy, "src/main/resources/no/edu/ntnu/idatt2001/magnusff/wargames/ArmyWrite.csv");

            Army readArmy = FileHandler.readFromCsv("src/main/resources/no/edu/ntnu/idatt2001/magnusff/wargames/ArmyWrite.csv");
            assertTrue(readArmy.getRangedUnits().get(0) instanceof RangedUnit);
        }

        @Test
        @DisplayName("CavalryUnit is of correct type")
        public void CavalryUnitIsOfCorrectType() throws IOException {
            Army writeArmy = new Army("Army one");
            CavalryUnit unit_1 = new CavalryUnit("Kåre", 15);
            CommanderUnit unit_2 = new CommanderUnit("Snikka", 50);
            writeArmy.add(unit_1);
            writeArmy.add(unit_2);
            FileHandler.writeToCsv(writeArmy, "src/main/resources/no/edu/ntnu/idatt2001/magnusff/wargames/ArmyWrite.csv");

            Army readArmy = FileHandler.readFromCsv("src/main/resources/no/edu/ntnu/idatt2001/magnusff/wargames/ArmyWrite.csv");
            assertTrue(readArmy.getCavalryUnits().get(0) instanceof CavalryUnit);
        }

        @Test
        @DisplayName("CommanderUnit is of correct type")
        public void CommanderUnitIsOfCorrectType() throws IOException {
            Army writeArmy = new Army("Army one");
            CommanderUnit unit_1 = new CommanderUnit("Kåre", 15);
            CavalryUnit unit_2 = new CavalryUnit("Snikka", 15);
            writeArmy.add(unit_1);
            writeArmy.add(unit_2);

            FileHandler.writeToCsv(writeArmy, "src/main/resources/no/edu/ntnu/idatt2001/magnusff/wargames/ArmyWrite.csv");

            Army readArmy = FileHandler.readFromCsv("src/main/resources/no/edu/ntnu/idatt2001/magnusff/wargames/ArmyWrite.csv");
            assertTrue(readArmy.getCommanderUnits().get(0) instanceof CommanderUnit);
        }
    }
}
