package no.edu.ntnu.idatt2001.magnusff.wargames;

import no.edu.ntnu.idatt2001.magnusff.wargames.units.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class UnitTest {


    @Test
    @DisplayName("Reduced resistbonus for the rangedUnit when attacked multiple times")

    public void rangedUnitReducedResistBonusWhenAttackedMultipleTimes() {
        RangedUnit unit_1 = new RangedUnit("Kåre", 50);
        assertEquals(6, unit_1.getResistBonus("Forrest"));
        assertEquals(4, unit_1.getResistBonus("Forrest"));
        assertEquals(2, unit_1.getResistBonus("Forrest"));
        assertEquals(2, unit_1.getResistBonus("Forrest"));
    }

    @Test
    @DisplayName("Reduced health for the infantryUnit when attacked")

    public void infantryUnitReducedHealthWhenAttacked() {
        InfantryUnit unit_1 = new InfantryUnit("Baggo", 15);
        InfantryUnit unit_2 = new InfantryUnit("Peder", 30);
        unit_1.attack(unit_2,"Forrest");
        assertEquals(24, unit_2.getHealth());
    }


    @Test
    @DisplayName("Attackbonus for the cavalryUnit")


    public void cavalryUnitAttackBonusReducedWhenAttackingMultipleTimes() {
        CavalryUnit ron = new CavalryUnit("Ron", 500);
        assertEquals(6, ron.getAttackBonus("Forrest"));
        assertEquals(2, ron.getAttackBonus("Forrest"));
        assertEquals(2, ron.getAttackBonus("Forrest"));
        assertEquals(2, ron.getAttackBonus("Forrest"));
    }

    @Test
    @DisplayName("CommanderUnits attackbonus gets reduced after first attack")
    public void commanderUnitAttackBonusReducedAfterAttackingMultipleTimes() {
        CavalryUnit baggo = new CavalryUnit("baggo", 500);
        assertEquals(6, baggo.getAttackBonus("Forrest"));
        assertEquals(2, baggo.getAttackBonus("Forrest"));
        assertEquals(2, baggo.getAttackBonus("Forrest"));
    }

    @Test
    @DisplayName("When health is lower than or equals to 0, exception is thrown and assertion succeeds")
    public void healthCantBeLowerThanZero() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            InfantryUnit baggo = new InfantryUnit("baggo", 0, 14, 15);

        });
        String expectedMessage = "Health cant be 0 or lower";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    @DisplayName("When attack is lower than or equals to 0, exception is thrown and assertion succeeds")
    public void attackCantBeLowerThanZero() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            InfantryUnit baggo = new InfantryUnit("baggo", 15, 0, 15);

        });
        String expectedMessage = "Attack cant be 0 or lower";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    @DisplayName("When armor is lower than or equals to 0, exception is thrown and assertion succeeds")
    public void armorCantBeLowerThanZero() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            InfantryUnit baggo = new InfantryUnit("baggo", 15, 15, 0);

        });
        String expectedMessage = "Armor cant be 0 or lower";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    @DisplayName("A Unit´s name cant be blank")
    public void unitsNameCantBeBlank(){
        Exception exception = assertThrows(IllegalArgumentException.class, () ->{
            InfantryUnit baggo = new InfantryUnit("", 15, 15 ,15);
        });
        String expectedMessage = "The solider needs a name!";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }
    @Test
    @DisplayName("UnitFactory creates correct unit based on input")
    public void UnitFactoryCreateUnit(){
        Unit unit1 = UnitFactory.createUnit("InfantryUnit", "baggo", 12);
        assertTrue(unit1 instanceof InfantryUnit);
    }
    @Test
    @DisplayName("A unit needs to be instance of a unit to be created")
    public void UnitFactoryNotCreateUnitWhenWrongUnitType(){
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            Army army = new Army("Baggouches");
            Unit unit1 = UnitFactory.createUnit("GolemUnit", "baggo", 12);
            army.add(unit1);
        });
        String expectedMessage = "The unit is of wrong unitType";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }
}