package no.edu.ntnu.idatt2001.magnusff.wargames;

import no.edu.ntnu.idatt2001.magnusff.wargames.units.Army;
import no.edu.ntnu.idatt2001.magnusff.wargames.units.InfantryUnit;
import no.edu.ntnu.idatt2001.magnusff.wargames.units.RangedUnit;
import no.edu.ntnu.idatt2001.magnusff.wargames.units.Unit;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.DisabledIfSystemProperties;

import static org.junit.jupiter.api.Assertions.*;

public class BattleTest {

    @Test
    @DisplayName("Simulation")

    void simulationTest() {
        Army armyone = new Army("Baggo");
        Army armytwo = new Army("Peder");

        InfantryUnit unit_1 = new InfantryUnit("Kent", 100);
        RangedUnit unit_2 = new RangedUnit("Kåre", 122);
        armyone.add(unit_1);
        armytwo.add(unit_2);
        Battle battle = new Battle(armyone, armytwo);
        battle.simulate("Forrest");
        assertFalse(armyone.hasUnits() & armytwo.hasUnits());

    }
}
