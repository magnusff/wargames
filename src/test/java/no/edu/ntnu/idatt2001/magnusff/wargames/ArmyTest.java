package no.edu.ntnu.idatt2001.magnusff.wargames;

import no.edu.ntnu.idatt2001.magnusff.wargames.units.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class ArmyTest {


    @Test
    @DisplayName("Adding a unit to an army, and checking if the hasUnits-method works")

    void addUnitToArmy() {
        Army army = new Army("Baggo");
        InfantryUnit peder = new InfantryUnit("Peder", 100);
        army.add(peder);
        assertTrue(army.hasUnits());
    }


    @Test
    @DisplayName("Adding a list of units to an army")

    void addAllunitsToArmy() {
        Army army = new Army("Baggo2");
        ArrayList <Unit> units = new ArrayList<>();
        InfantryUnit Peder = new InfantryUnit("Peder", 111);
        RangedUnit bagnes = new RangedUnit("bagnes", 200);
        CavalryUnit kent = new CavalryUnit("Kent", 200);
        CommanderUnit perodd = new CommanderUnit("Perodd", 20000);
        units.add(Peder);
        units.add(bagnes);
        units.add(kent);
        units.add(perodd);
        army.addAll(units);
        assertTrue(army.hasUnits());
    }


    @Test
    @DisplayName("Removing a unit from an army")
    void removeUnitFromArmy() {
        Army army = new Army("baggo");
        InfantryUnit Peder = new InfantryUnit("Peder", 222);
        army.add(Peder);
        assertTrue(army.remove(Peder));
    }


    @Test
    @DisplayName("Getting all units in a army")


    void getAllUnitsFromArmy() {
        Army army_1 = new Army("Baggo");
        InfantryUnit Peder = new InfantryUnit("Peder", 111);
        RangedUnit bagnes = new RangedUnit("bagnes", 200);
        CavalryUnit kent = new CavalryUnit("Kent", 200);
        CommanderUnit perodd = new CommanderUnit("Perodd", 20000);
        army_1.add(Peder);
        army_1.add(bagnes);
        army_1.add(kent);
        army_1.add(perodd);
        assertEquals(4, army_1.getAllUnits().size());

    }

    @Test
    @DisplayName("Getting a random unit from an army")

    void getRandomUnitFromArmy() {
        Army army_1 = new Army("Baggo");
        InfantryUnit Peder = new InfantryUnit("Peder", 111);
        RangedUnit bagnes = new RangedUnit("bagnes", 200);
        CavalryUnit kent = new CavalryUnit("Kent", 200);
        CommanderUnit perodd = new CommanderUnit("Perodd", 20000);
        army_1.add(Peder);
        army_1.add(bagnes);
        army_1.add(kent);
        army_1.add(perodd);
        assertTrue(army_1.getRandomUnit() instanceof Unit);
    }

        @Test
        @DisplayName("Getting all cavalryUnits from an army")
        void getCavalryUnitsFromArmy() {
            Army army = new Army("baggo");
            CavalryUnit Peder = new CavalryUnit("Peder", 222);
            CommanderUnit Baggo = new CommanderUnit("Baggnes", 100000);
            army.add(Peder);
            army.add(Baggo);
            assertEquals(1,army.getCavalryUnits().size());
        }
    @Test
    @DisplayName("Getting all infantryUnits from an army")
    void getInfantryUnitsFromArmy() {
        Army army = new Army("baggo");
        InfantryUnit Peder = new InfantryUnit("Peder", 222);
        CommanderUnit baggo = new CommanderUnit("baggo", 15);
        army.add(Peder);
        assertEquals(1,army.getInfantryUnits().size());

    }
    @Test
    @DisplayName("Getting all rangedUnits from an army")
    public void getRangedUnitsFromArmy(){
        Army army = new Army("Baggo");
        RangedUnit Magnus = new RangedUnit("Peder", 15);
        RangedUnit Oskar = new RangedUnit("Oskar", 15);
        InfantryUnit Kare = new InfantryUnit("Kåre", 20);
        army.add(Magnus);
        army.add(Oskar);
        army.add(Kare);
        assertEquals(2,army.getRangedUnits().size());
    }
    @Test
    @DisplayName("Checking if a army is out of units")
    public void armyHasNoUnitsLeftAfterWar(){
        Army army1 = new Army("baggo");
        RangedUnit magnus = new RangedUnit("magnus", 1, 15, 10);
        army1.add(magnus);

        Army army2 = new Army("oskar");
        RangedUnit oskar = new RangedUnit("oskar", 400,15,15);
        army2.add(oskar);

        Battle battle = new Battle(army1, army2);

        System.out.println(battle.simulate("Forrest"));

        assertFalse(army1.hasUnits());

        }
    }